import { Component, OnInit, Input, Inject } from '@angular/core';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  private rollno: number;
  private name: string;
  private email: string;
  private password: string;
  private mobno: number;
  private countries: string[] = ["India", "Pakisthan", "China", "USA"];
  display: boolean = false;

  ngOnInit() {
  }

  onSubmit(data: any) {
    this.display = true;
    console.log(data.value);// output with key and value(means propertyName and value)
    console.log(data.controls);// output all objects
    if (data.controls.name.value === 'raju') {
      // this.display = false;
      this.name = data.controls.name.value;// stored the value of name-property.
      console.log(this.name);
    }
  }

}
